import subprocess
with open("hosts_and_ports.txt") as hp_fh:
        hp_contents = hp_fh.readlines()

        for hp_pair in hp_contents:
                lst = hp_pair.split(':')
                
                with open("command.txt") as fh:
                        completed = subprocess.run(f"ssh ubuntuserver@{lst[0]} -p {lst[1]}", capture_output=True, text=True, shell = True, stdin=fh)
