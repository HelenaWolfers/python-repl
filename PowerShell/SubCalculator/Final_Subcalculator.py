# OS-Scripting - ExamenProject NetwerkCalculator
# Helena Wolfers - 1DSN1
#
#
import sys


# Stap 1: een IPv4-hostadres opvangen
#
#
#
# Stap 2: een subnetmasker opvangen
#
#
#
def ask_for_number_sequence(message):
    user_input = input(message)
    octet_split = user_input.split('.')

    numberlist = [int(octet) for octet in octet_split]
    ###TEST### print (numberlist)
    
    return numberlist




# Stap 3a: geldigheid controleren IP-adres
#
# 
#
def is_valid_ip_address(numberlist):

    if (len(numberlist) == 4) and (numberlist[0] != 127) and (numberlist[0] != 169) and (0 <= numberlist[0] <= 255) and (0 <= numberlist[1] <= 255) and (0 <= numberlist[2] <= 255) and (0 <= numberlist[3] <= 255):
        return True
    else:
        return False
        
        


# Stap 3b - Geldigheid controleren subnet-adres
# 
# 
#
def is_valid_subnet(numberlist):
    # Klass A (submetmask= 255.0.0.0)
    # Klass B (subnetmask = 255.255.0.0 - 255.254.0.0 - 255.252.0.0 - 255.248.0.0 - 255.240.0.0 - 255.224.0.0 - 255.192.0.0 - 255.128.0.0)
    # Klass C (subnetmask = 255.255.255.0 - 255.255.254.0 - 255.255.252.0 - 255.255.248.0 - 255.255.240.0 - 255.255.224.0 - 255.255.192.0 - 255.255.128.0)
    # Klass D (subnetmask = 255.255.255.255 - 255.255.255.254 - 255.255.255.252 - 255.255.255.248 - 255.255.255.240 - 255.255.255.224 - 255.255.255.192 - 255.255.255.128)
    masks = [0, 128, 192, 224, 240, 248, 252, 254, 255]
        
    if (len(numberlist) == 4) and (numberlist[0] == 255) and (numberlist[1] in masks) and (numberlist[2] in masks) and (numberlist[3] in masks) and (numberlist[0] >= numberlist[1] >= numberlist[2] >= numberlist[3]):
        is_valid_binary_subnet(numberlist)
    else:
        return False




# Stap 3c - Octet uit subnet adres omzetten naar binair + eventuele aanvulling van 0 naar 8 bits
# 
# 
#
def is_valid_binary_subnet(numberlist):

    binary_numberlist = []
    binary_numberlist = [bin(i).split("b")[1] for i in numberlist]
    ####TEST#### print ("BINARY-NUMBERLIST", binary_numberlist)

    binary_netmask_numberlist = []

    for bin_number in range(0,len(binary_numberlist)):
        if len (binary_numberlist[bin_number]) < 8:
            add_zeros = binary_numberlist[bin_number].zfill(8)
            binary_netmask_numberlist.append(add_zeros)
        else:
            binary_netmask_numberlist.append(binary_numberlist[bin_number])
    ####TEST#### print ("NETMASK_NUMBERLIST: ", binary_netmask_numberlist)
    return binary_netmask_numberlist

    


# Stap 3d - Geldigheid controleren netmask-adres
# 
# 
#
def is_valid_netmask(numberlist):
    binary_netmask_numberlist = is_valid_binary_subnet(numberlist)
    binary_netmask = "".join(binary_netmask_numberlist)
    ####TEST#### print ("STRING BINARY_NETMASK: ", binary_netmask)

    # BEETJE ANDERS GEDAAN NIET DUIDELIJK WAT DE OPDRACHT WAS
    checking_ones = True

    while checking_ones:
        if binary_netmask.index("0"):
            checking_ones = False
        elif binary_netmask.index("1") and checking_ones == False:
            print("Het netmasker is ongeldig")
            return False
    return checking_ones
   



# Stap 4 - Aantal bits tellen  
#
#
def one_bits_in_netmask(numberlist):
    binary_netmask_numberlist = is_valid_binary_subnet(numberlist)
    #KRIJG HEM NIET AAN DE GANG... LIJST NETMASK_NUMBERLIST IS GEVULD MAAR BLIJF '0' KRIJGEN
    ####TEST#### print ("NETMASK_NUMBERLIST: ", binary_netmask_numberlist) 

    #IN ORDE GEKOMEN... 
    binary_netmask = "".join(binary_netmask_numberlist)

    counter = 0
    
    for number in binary_netmask:
        if number == "1":
            counter += 1
    return counter
    



# def one_bits_in_netmask_OPTIE_2(numberlist):
#     binary_netmask_numberlist = is_valid_binary_subnet(numberlist)
#     ####TEST#### print ("NETMASK_NUMBERLIST: ", binary_netmask_numberlist) 
    
#     binary_netmask = "".join(binary_netmask_numberlist)
#     # NIET GEVRAAGD, MAAR HET WERKT!!!
#     # Waarom moeilijk doen, als makkelijk ook kan?
#     # String met binaire getallen, waarvan je de eentjes telt.
#     count = binary_netmask.count("1")
#     return count




def apply_network_mask(host_address, netmask):
    network_mask_list = []

    for i in range(4):
        network_mask_list.append(host_address[i] & netmask[i])
    
    return network_mask_list




#Stap 6 - wildcardmasker berekenen
#
#
#
def netmask_to_wilcard_mask(numberlist):
        
    wild_mask_list= []

    for i in numberlist:
        wild_bit = 255 - i
        wild_mask_list.append(wild_bit)
            
    return wild_mask_list




# Stap 7: broadcastadres berekenen
#
#
#
# vb. IP 128.0.0.0 / WILD 0.255.255.255 = 128.255.255.255
def get_broadcast_address(network_address, wildcard_mask):

    broadcast_list = []

    for i in range(4):
        broadcast_list.append(network_address[i] | wildcard_mask[i])

    #ToString_broadcast = ".".join([str(i) for i in broadcast_list])
    return broadcast_list




# Stap 8: 
#
#
#
def prefix_length_to_max_hosts(prefix):
    
    max_hosts = (2**(32-prefix)-2)
    return max_hosts




# Stap 9: ToString()
#
#
#
def to_string(numberlist):
    toString = ".".join([str(i) for i in numberlist])
    return toString

#####################################################################################################
########################################################
############################
##############
# Stap 1: een IPv4-hostadres opvragen 
#
#
#
ip_address = ask_for_number_sequence("Wat is het IP-adres?")
# Stap 2: een subnetmasker opvragen
#
#
#
subnetmask_address = ask_for_number_sequence("Wat is het subnetmasker?")

if is_valid_ip_address(ip_address) == True or is_valid_subnet(subnetmask_address) == True:
    print("IP-adres en subnetmasker zijn geldig.")

    length_subnetmask = one_bits_in_netmask(subnetmask_address)
    print (f"De lengte van het subnetmasker is {length_subnetmask}.")
    # OPTIE 2
    # length_subnetmask_optie_2 = one_bits_in_netmask_OPTIE_2(subnetmask_address)
    # print (f"De lengte van het subnetmasker is {length_subnetmask_optie_2}.")
    
    subnet_address = apply_network_mask(ip_address, subnetmask_address)
    print (f"Het adres van het subnet is {to_string(subnet_address)}.")

    wildcard = netmask_to_wilcard_mask(subnetmask_address)
    print (f"Het wildcardmasker is {to_string(wildcard)}.")

    broadcast = get_broadcast_address(subnet_address, wildcard)
    print (f"Het broadcastadres is {to_string(broadcast)}.")

    max_hosts = prefix_length_to_max_hosts(length_subnetmask)
    print(f"Het maximaal aantal hosts op dit subnet is {max_hosts}.")

else:
    print("IP-adres en/of subnetmasker is ongeldig.")
    sys.exit(0)
