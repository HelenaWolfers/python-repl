﻿
New-ModuleManifest -Path C:\Users\helen\Documents\WindowsPowerShell\Modules\ConnectTeams\MSTeamAssignment.psd1 -Author 'Helena Wolfers' -Description 'Aanmaken van kanalen op Teams vanuit een CSV-file'


function New_CustomChannel
{
[CmdletBinding()]

    param
    (
        [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
        [string]$newChannelName,
        [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
        [string]$newGroupId
    )

    Process
    {
        New-TeamChannel -GroupId $newGroupId -DisplayName $newChannelName 
    }
    
}