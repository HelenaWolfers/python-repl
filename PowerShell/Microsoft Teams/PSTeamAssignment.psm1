﻿
#New-ModuleManifest -Path C:\Users\helen\Documents\WindowsPowerShell\Modules\ConnectTeams\PSTeamAssignment.psd1 -Author 'Helena Wolfers' -Description 'Aanmaken van kanalen op Teams vanuit een CSV-file'
#Import-Module -Name PSTeamAssignment -Force



function New_CustomChannel
{
[CmdletBinding()]

    param
    (
        [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
        [string]$ChannelName,
        [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
        [string]$GroupID
    )

    Process
    {
        New-TeamChannel -GroupID $groupId -DisplayName $ChannelName 
    }
    
}