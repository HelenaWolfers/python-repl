﻿$varCredential = Get-Credential
Connect-MicrosoftTeams -Credential $varCredential

Get-TeamChannel -GroupId fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736 | Write-Output

$rows = Import-Csv -Path "C:\Users\helen\Documents\AP 2019-2020\OS Scripting\newchannels.csv"

foreach($row in $rows)
{
    New-Teamchannel -GroupId fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736 -DisplayName $row.ChannelName
}
